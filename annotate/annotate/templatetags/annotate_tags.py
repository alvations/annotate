"""Project wide templatetags."""
from django import template
from django.utils.safestring import mark_safe


register = template.Library()


@register.assignment_tag
def get_user_name(user):
    if user.first_name and user.last_name:
        return u'{} {}'.format(user.first_name, user.last_name)
    if user.first_name:
        return user.first_name
    if user.last_name:
        return user.last_name
    return user.email


@register.assignment_tag
def get_email_styles():
    return {
        'font_family': ("font-family: 'Helvetica Neue', Helvetica, Arial,"
                        " sans-serif;"),
        'font_size_xs': 'font-size: 11px; line-height: 14px;',
        'font_size_sm': 'font-size: 13px; line-height: 16px;',
        'font_size_md': 'font-size: 15px; line-height: 18px;',
        'font_size_lg': 'font-size: 18px; line-height: 22px;',
        'font_size_xl': 'font-size: 24px; line-height: 30px;',
        'border': '1px solid #eeeeee',
        'font_color': 'color: #333333;',
        'link_color': 'color: #2BADCA;',
        'color': '#2BADCA',
        'table': ('border-spacing: 0px; margin: 10px 0 20px; text-align: left;'
                  ' width: 100%; max-width: 100%;'),
        'table_th': ('border-bottom: 1px solid #eee; vertical-align: top;'
                     ' padding: 10px 0; white-space: nowrap;'),
        'table_td': ('border-bottom: 1px solid #eee; vertical-align: top;'
                     ' padding: 10px 0  10px 20px;'),
        'hr': mark_safe('<div style="border-top: 1px solid #eeeeee;'
                        ' margin: 25px 0 10px;"></div>'),
        'clearfix': mark_safe('<div style="clear: both;"></div>'),
    }
