"""Views for the annotate project."""
import re

from django.views.generic import TemplateView

from allauth.account.views import LoginView, SignupView


class AccountViewMixin(object):
    def get_form_kwargs(self):
        kwargs = super(AccountViewMixin, self).get_form_kwargs()
        if self.request.GET.get('next'):
            match = re.search(r'[\w\.-]+@[\w\.-]+', self.request.GET['next'])
            if match and match.group(0):
                kwargs['initial']['email'] = match.group(0)
        return kwargs


class AnnotateLoginView(AccountViewMixin, LoginView):
    pass


class AnnotateSignupView(AccountViewMixin, SignupView):
    pass


class ContentTemplateView(TemplateView):
    def get_template_names(self):
        return u'content/{}.html'.format(self.kwargs['template'])
