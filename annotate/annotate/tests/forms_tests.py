"""Tests for the forms of the ``annotate`` project."""
from django.test import TestCase

from mixer.backend.django import mixer

from .. import forms


class SignupFormTestCase(TestCase):
    """Test for the ``SignupForm`` form class."""
    longMessage = True

    def test_validates_saves_and_sends_email(self):
        data = {
            'email': 'test1@example.com',
            'password1': 'foobar1',
            'first_name': 'Foo',
            'last_name': 'Bar',
        }
        form = forms.SignupForm(data=data)
        self.assertFalse(form.errors)

        mixer.blend('auth.User', email='test1@example.com')
        data = {
            'email': 'test1@example.com',
            'password1': 'foo',
            'first_name': 'Foo',
            'last_name': 'Bar',
        }

        form = forms.SignupForm(data=data)
        self.assertFalse(form.is_valid())
