"""Tests for the views of the ``annotate`` project."""
from django.test import TestCase

from django_libs.tests.views_tests import ViewRequestFactoryTestMixin

from .. import views


class AnnotateLoginViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.AnnotateLoginView

    def test_view(self):
        self.is_callable()
        self.is_callable(data={'next': '/'})
        self.is_callable(data={'next': '/?email=foo@example.com'})


class ContentTemplateViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.ContentTemplateView

    def get_view_kwargs(self):
        return {'template': 'why'}

    def test_view(self):
        self.is_callable()
