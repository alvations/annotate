"""Tests for the template tags and filters of the annotate project."""
from django.test import TestCase

from mixer.backend.django import mixer

from ..templatetags import annotate_tags


class GetUserNameTestCase(TestCase):
    def test_tag(self):
        user = mixer.blend('auth.User', first_name='', last_name='')
        self.assertEqual(annotate_tags.get_user_name(user), user.email)
        user.last_name = 'Bar'
        self.assertEqual(annotate_tags.get_user_name(user), 'Bar')
        user.first_name = 'Foo'
        user.last_name = 'Bar'
        self.assertEqual(annotate_tags.get_user_name(user), 'Foo Bar')
        user.last_name = ''
        self.assertEqual(annotate_tags.get_user_name(user), 'Foo')


class GetEmailStylesTestCase(TestCase):
    def test_tag(self):
        self.assertTrue(annotate_tags.get_email_styles())
