"""Settings for the ``impersonate`` app."""
IMPERSONATE_ALLOW_SUPERUSER = True
IMPERSONATE_REDIRECT_FIELD_NAME = 'next'
