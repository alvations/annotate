"""Forms for the ``annotate`` project."""
from django import forms
from django.core.urlresolvers import reverse
from django.template.defaultfilters import mark_safe
from django.utils.translation import ugettext_lazy as _

from allauth.account.forms import SignupForm as AllauthSignupForm


class SignupForm(AllauthSignupForm):
    """Customized signup form to add book data."""
    first_name = forms.CharField(label=_('First name'))
    last_name = forms.CharField(label=_('Last name'))
    field_order = ['first_name', 'last_name', 'email', 'password1']

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.fields['email'].label = _('Email')
        # Add bootstrap's placeholder attribute
        self.fields.pop('password2')
        for field in self.fields:
            attrs = {'placeholder': self.fields[field].label}
            if field in ['password1', ]:
                self.fields[field].widget = forms.PasswordInput(
                    attrs=attrs, render_value=False)
            elif field not in ['category', 'confirmation_key']:
                self.fields[field].widget = forms.TextInput(attrs=attrs)

    def raise_duplicate_email_error(self):
        raise forms.ValidationError(mark_safe(_(
            'You are already registered. Please <a href="{}">login</a> using'
            ' this email address.'.format(reverse('account_login')))))
