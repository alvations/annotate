function init_arrow($token1, $token2, marker, label_pk, pk, delete_url) {
    var path,
        $scope = $('[data-id="scope"]');

    $scope.$svg = $("svg");
    $scope.svg = $scope.$svg[0];

    // Curve configuration
    var curve_distance = 20;
    var y_offset = 5;

    // Positions
    var token1_pos_top = $token1.offset().top - $scope.$svg.offset().top;
    var token1_pos_left = $token1.offset().left - $scope.$svg.offset().left;
    var token2_pos_top = $token2.offset().top - $scope.$svg.offset().top;
    var token2_pos_left = $token2.offset().left - $scope.$svg.offset().left;

    // Corrected positions
    var token1_pos_shifted_left = token1_pos_left + $token1.width() / 4;
    var token1_pos_shifted_top = token1_pos_top - y_offset;
    var token2_pos_shifted_left = token2_pos_left + $token2.width() / 4;
    var token2_pos_shifted_top = token2_pos_top - y_offset;

    // Format should be:
    // M<x1>,<y1> C<x1>,<y1 + distance> <x2>,<y2 + distance> <x2>,<y2>
    var curve_string = "M{0},{1} C{0},{4} {2},{5} {2},{3}".format(
        Math.round(token1_pos_shifted_left), // {0}
        Math.round(token1_pos_shifted_top),  // {1}
        Math.round(token2_pos_shifted_left), // {2}
        Math.round(token2_pos_shifted_top),  // {3}
        Math.round(token1_pos_shifted_top - curve_distance), // {4}
        Math.round(token2_pos_shifted_top - curve_distance)  // {5}
    );

    path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    path.setAttribute("class", 'arrow');
    path.setAttribute("data-delete-url", delete_url);
    path.setAttribute("data-pk", pk);
    path.setAttribute("data-label-pk", label_pk);
    path.setAttribute("style", 'marker-end: url(#' + marker.attr('id') + '); stroke: ' + marker.css('fill') + ';');
    path.setAttribute("d", curve_string);
    $scope.svg.appendChild(path);
    $('[data-class="word"].selected').removeClass('selected');

    // Add label
    var text = document.createElementNS("http://www.w3.org/2000/svg", "text");
    var box = path.getBBox();
    text.setAttribute("transform", "translate(" + (box.x + box.width/2 - 7) + " " + (box.y + box.height/2 - 9) + ")");
    text.textContent = marker.data('name');
    text.setAttribute("fill", marker.css('fill'));
    text.setAttribute("font-size", "12");
    text.setAttribute("data-pk", pk);
    text.setAttribute("data-label-pk", label_pk);
    path.parentNode.insertBefore(text, path.nextSibling);

    // Add deletion function
    $(path).click(function() {
        var pk = $(this).data('pk');
        $.post($(this).data('delete-url'), [
            {name: 'csrfmiddlewaretoken', value: getCookie('csrftoken')}
        ], function() {
            $('path[data-pk="' + pk + '"], text[data-pk="' + pk + '"]').remove();
        });
    });
}

$(document).ready(function () {
    $('[data-class="word"]').click(function() {
        if (!$('[data-class="word"].selected').length) {
            // If there's no selected word, this should be the first one
            $(this).addClass('selected');
        } else {
            if ($(this).hasClass('selected')) {
                // You're de-selecting the one you've just selected
                $(this).removeClass('selected');
            } else {
                var $token1 = $('[data-class="word"].selected').not($(this));
                var $token2 = $(this);

                if ($token1.parents('.sentence').data('sentence-id') == $token2.parents('.sentence').data('sentence-id')) {
                    // Users are only allowed to link words in the same sentence
                    var active_marker = $('.arrow-point.selected');
                    $.post(active_marker.data('create-url'), [
                        {name: 'csrfmiddlewaretoken', value: getCookie('csrftoken')},
                        {name: 'sentence', value: $token1.parents('.sentence').data('sentence-id')},
                        {name: 'first_word', value: $token1.text()},
                        {name: 'first_word_position', value: $token1.data('position')},
                        {name: 'second_word', value: $token2.text()},
                        {name: 'second_word_position', value: $token2.data('position')}
                    ], function(json) {
                        json = $.parseJSON(json);
                        init_arrow($token1, $token2, active_marker, json['label_pk'], json['pk'], json['delete_url']);
                    });
                }
            }
        }
    });

    // Change label
    $('[data-class="change-label"]').click(function(e) {
        e.preventDefault();
        var label = $(this);
        $('[data-class="change-label"]').parents('li').removeClass('disabled');
        label.parents('li').addClass('disabled').parents('.dropdown').find('button').html(label.html() + ' <span class="caret"></span>');

        // Change the colour of the arrows
        $('.arrow-point').attr('class', 'arrow-point');
        $('#arrow-point-' + label.data('label-pk')).attr('class', 'arrow-point selected');
    });

    // Init arrows
    $('.sentence').each(function() {
        var sentence = $(this),
            annotations = sentence.data('annotations');
        for (var i in annotations) {
            init_arrow(sentence.find('[data-class="word"][data-position="' + annotations[i]['first_word'] + '"]'), sentence.find('[data-class="word"][data-position="' + annotations[i]['second_word'] + '"]'), $('#arrow-point-' + annotations[i]['label']), annotations[i]['label'], annotations[i]['pk'], annotations[i]['delete_url']);
        }
    });
});

String.prototype.format = String.prototype.f = function () {
    var s = this;
    var i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};
