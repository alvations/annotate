/*jshint unused: false*/
function get_screen_size() {
    // Returns the current screen size in Bootstrap 3 terms
    // See http://stackoverflow.com/a/19462847
    //
    var envValues = ['xs', 'sm', 'md', 'lg'];
    var $el = $('<div></div>');
    $el.appendTo($('body'));
    for (var i = envValues.length - 1; i >= 0; i--) {
        $el.addClass('hidden-'+envValues[i]);
        if ($el.is(':hidden')) {
            $el.remove();
            return envValues[i];
        }
    }
}
/*jshint unused: true*/

function init_carousel(elements) {
    // Carousel
    elements.carousel({
        interval: 7000
    });
}

function init_popover(elements) {
    // Popover
    elements.popover({
        placement: 'top'
        ,trigger: 'hover'
        ,html: true
    }).css('cursor', 'pointer');
}

function init_textarea(elements) {
    // Textarea auto-expand
    elements.css('overflow', 'hidden').autogrow();
}

function init_tabs() {
    // show active tab on reload
    if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');

    // remember the hash in the URL without jumping
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        if(history.pushState) {
            history.pushState(null, null, '#'+$(e.target).attr('href').substr(1));
        } else {
            location.hash = '#'+$(e.target).attr('href').substr(1);
        }
    });
}

function init_tooltip(elements) {
    // Tooltip
    elements.tooltip({
        placement: 'bottom'
    });
}

function init_spinner(elements) {
    // Spinner
	elements.spin({
		lines: 11
		,length: 5
		,width: 4
		,radius: 10
		,corners: 1.0
		,rotate:0
		,trail: 62
		,speed: 1.0
		,direction: 1
	});
}

function init_label_forms() {
    $('[data-class="label-form"]').unbind('submit').on('submit', function(e) {
        e.preventDefault();
    });
    $('[data-class="label-form"] [type="submit"]').unbind('click').on('click', function(e) {
        e.preventDefault();
        var btn = $(this),
            label_group = btn.parents('.form-group');

        label_group.find('button').prop('disabled', true);
        label_group.find('input').prop('readonly', true);

        $.post(btn.data('action'), [
            {name: 'csrfmiddlewaretoken', value: getCookie('csrftoken')}
            ,{name: 'name', value: label_group.find('[name="name"]').val()}
        ], function(html) {
            label_group.replaceWith(html);
            init_label_forms();
        });
    });
}

function init_form(form) {
    if (!form.find('[data-class="spinner"]').length) form.prepend('<div class="spinner-wrapper" data-class="spinner"></div>');
	form.change(function() {
		if (form.find('input[type=file]').length) {
			var input, file;

			// (Can't use `typeof FileReader === "function"` because apparently
			// it comes back as "object" on some browsers. So just see if it's there
			// at all.)
			if (!window.FileReader) {
				$('<div class="alert alert-danger">' + gettext("The file API isn't supported on this browser yet.") + '</alert>').insertAfter(input);
				return;
			}

			input = form.find('input[type=file]');
			if (!input[0].files) {
				$('<div class="alert alert-danger">' + gettext("This browser doesn't seem to support the 'files' property of file inputs.") + '</div>').insertAfter(input);
			}
			else if (!input[0].files[0]) {
				input.parents('.form-group').find('.alert').remove();
			}
			else {
				file = input[0].files[0];
				// 5MB -> 5120KB -> 5242880
				if (file.size > 5242880) {
					$('<div class="alert alert-danger">' + gettext('The file is too large. Please select a smaller image (< 5mb).') + '</div>').insertAfter(input);
					input[0].parentNode.replaceChild(
						input[0].cloneNode(true),
						input[0]
					);
				} else {
					input.parents('.form-group').find('.alert').remove();
				}
			}
		}
	});
    form.submit(function(e) {
        form.find('[data-class="spinner"]').show();
    });
	init_spinner($('[data-class="spinner"]'));
}

// Document loaded and ready.
$(document).ready(function() {
    init_popover($('[data-class="popover"]'));
    init_tabs();
    init_tooltip($('[data-class="tooltip"]'));
    init_textarea($('textarea'));
	init_spinner($('[data-class="spinner"]'));
	$('form').each(function() {
		init_form($(this));
	});
    // Use this in in the template that has a carousel slider
    // init_carousel($('[data-class="carousel"]'));

	// Disabled links
	$('body').on('click', 'a.disabled', function(event) {
		event.preventDefault();
	});

    // Label management
    init_label_forms();

    // Affix
    $('[data-affix]').each(function() {
        var element = $(this);
        element.css('max-width', element.parents('div').width());
        if ($('[data-id="main-container"]').outerHeight() > $(this).outerHeight()) {
            // Only enable affix, if the body part is large enough
            element.affix({
                offset: {
                    top: function () {
                        return (this.top = element.offset().top)
                    }
                }
            });
        }
    });
});

// If objects are added to the DOM after document ready.
$(document).on('DOMNodeInserted', function(e) {
    init_popover($(e.target).find('[data-class="popover"]'));
    init_tooltip($(e.target).find('[data-class="tooltip"]'));
    init_textarea($(e.target).find('textarea'));
	init_spinner($(e.target).find('[data-class="spinner"]'));
	$(e.target).find('form').each(function() {
		init_form($(this));
	});
    init_carousel($(e.target).find('[data-class="carousel"]'));
});

// File upload
$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({});

	// Load existing files:
	$('#fileupload').addClass('fileupload-processing');
	$.ajax({
		url: $('#fileupload').data('json-url'),
		dataType: 'json',
		context: $('#fileupload')[0]
	}).always(function () {
		$(this).removeClass('fileupload-processing');
	}).done(function (result) {
		$(this).fileupload('option', 'done')
			.call(this, null, {result: result});
	});
});

$('html').ajaxSend(function(event, xhr, settings) {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
        // Only send the token to relative URLs i.e. locally.
        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
    }
});

window.locale = {
    "fileupload": {
        "errors": {
            "maxFileSize": "File is too big",
            "minFileSize": "File is too small",
            "acceptFileTypes": "Filetype not allowed",
            "maxNumberOfFiles": "Max number of files exceeded",
            "uploadedBytes": "Uploaded bytes exceed file size",
            "emptyResult": "Empty file upload result"
        },
        "error": "Error",
        "start": "Start",
        "cancel": "Cancel",
        "destroy": "Delete",
        "label": "Start labelling"
    }
};
