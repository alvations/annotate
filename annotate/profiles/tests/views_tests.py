"""Tests for the views of the profiles app."""
from django.test import TestCase

from django_libs.tests.mixins import ViewRequestFactoryTestMixin
from mixer.backend.django import mixer

from .. import views


class ProfileViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.ProfileView

    def setUp(self):
        self.user = mixer.blend('auth.User')

    def test_view(self):
        self.is_callable(user=self.user)
