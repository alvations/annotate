"""Tests for the models of the profiles app."""
from django.contrib.auth import get_user_model
from django.test import TestCase

from mixer.backend.django import mixer

from .. import models


class SignalTestCase(TestCase):
    def test_user_signup(self):
        user = mixer.blend('auth.User', first_name='', last_name='')
        for x in range(0, 4):
            invitation = mixer.blend('projects.Invitation', email=user.email,
                                     first_name='', last_name='')
            invitation.user = None
            if x == 1:
                invitation.first_name = 'Foobar'
            elif x == 2:
                invitation.first_name = 'Foo'
                invitation.last_name = 'Bar'
            invitation.save()
        models.allauth_user_signed_up({}, user)
        self.assertEqual(
            get_user_model().objects.get(pk=user.pk).get_full_name(),
            'Foobar Bar')
