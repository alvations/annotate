"""Models for the profile app."""
from allauth.account.signals import user_signed_up

from projects.models import Invitation


def allauth_user_signed_up(request, user, **kwargs):
    # Assign invitations
    invitations = Invitation.objects.filter(
        email=user.email, user__isnull=True)
    if not user.first_name:
        for invitation in invitations:
            if invitation.first_name:
                user.first_name = invitation.first_name
                break
    if not user.last_name:
        for invitation in invitations:
            if invitation.last_name:
                user.last_name = invitation.last_name
                break
    user.save()
    invitations.update(user=user)
user_signed_up.connect(allauth_user_signed_up)
