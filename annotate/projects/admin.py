"""Admin models for the projects app."""
from django.contrib import admin

from . import models


admin.site.register(models.Annotation)
admin.site.register(models.File)
admin.site.register(models.Invitation)
admin.site.register(models.Label)
admin.site.register(models.Project)
