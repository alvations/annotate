"""Tests for the template tags and filters of the project app."""
from django.test import TestCase

from ..templatetags import project_tags


class UploadJSTestCase(TestCase):
    def test_tag(self):
        self.assertTrue(project_tags.upload_js())
