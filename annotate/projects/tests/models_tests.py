"""Tests for the models of the projects app."""
from django.test import TestCase

from mixer.backend.django import mixer


class ProjectTestCase(TestCase):
    def setUp(self):
        self.project = mixer.blend('projects.Project')

    def test_model(self):
        self.assertTrue(str(self.project))


class FileTestCase(TestCase):
    def setUp(self):
        self.file = mixer.blend('projects.File')

    def test_model(self):
        self.assertTrue(str(self.file))

    def test_deletion(self):
        self.file.delete()


class InvitationTestCase(TestCase):
    def setUp(self):
        self.invitation = mixer.blend('projects.Invitation')

    def test_model(self):
        self.assertTrue(str(self.invitation))


class LabelTestCase(TestCase):
    def setUp(self):
        self.label = mixer.blend('projects.Label')

    def test_model(self):
        self.assertTrue(str(self.label))


class AnnotationTestCase(TestCase):
    def setUp(self):
        self.annotation = mixer.blend('projects.Annotation')

    def test_model(self):
        self.assertTrue(str(self.annotation))
