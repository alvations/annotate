"""Forms for the projects app."""
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase

from mixer.backend.django import mixer, MOCK_FILE

from .. import forms


class ProjectFormTestCase(TestCase):
    def test_form(self):
        owner = mixer.blend('auth.User')
        form = forms.ProjectForm(owner=owner, data={})
        self.assertFalse(form.is_valid(), msg=('Form should be invalid.'))
        form = forms.ProjectForm(owner=owner, data={'name': 'Foo'})
        self.assertTrue(form.is_valid(), msg=(
            'Form should be valid. Errors: {}'.format(form.errors)))
        form.save()


class FileFormTestCase(TestCase):
    def test_form(self):
        with open(MOCK_FILE, 'rb') as f:
            project = mixer.blend('projects.Project')
            form = forms.FileForm(project=project, data={})
            self.assertFalse(form.is_valid(), msg=('Form should be invalid.'))
            form = forms.FileForm(
                project=project,
                data={},
                files={'file': SimpleUploadedFile(f.name, f.read())}
            )
            self.assertTrue(form.is_valid(), msg=(
                'Form should be valid. Errors: {}'.format(form.errors)))
            form.save()


class InvitationFormTestCase(TestCase):
    def test_form(self):
        project = mixer.blend('projects.Project')
        user = mixer.blend('auth.User')
        form = forms.InvitationForm(project=project, data={})
        self.assertFalse(form.is_valid(), msg=('Form should be invalid.'))
        form = forms.InvitationForm(project=project, data={
            'email': project.owner.email})
        self.assertFalse(form.is_valid(), msg=('Form should be invalid.'))
        form = forms.InvitationForm(project=project, data={
            'email': user.email})
        self.assertTrue(form.is_valid(), msg=(
            'Form should be valid. Errors: {}'.format(form.errors)))
        form.save()
        form = forms.InvitationForm(project=project, data={
            'email': 'foo@example.com'})
        self.assertTrue(form.is_valid(), msg=(
            'Form should be valid. Errors: {}'.format(form.errors)))
        form.save()
        form = forms.InvitationForm(project=project, data={
            'email': 'foo@example.com'})
        self.assertFalse(form.is_valid(), msg=('Form should be invalid.'))


class LabelFormTestCase(TestCase):
    def test_form(self):
        project = mixer.blend('projects.Project')
        form = forms.LabelForm(project=project, data={})
        self.assertFalse(form.is_valid(), msg=('Form should be invalid.'))
        form = forms.LabelForm(project=project, data={'name': 'Foo'})
        self.assertTrue(form.is_valid(), msg=(
            'Form should be valid. Errors: {}'.format(form.errors)))
        form.save()
        form = forms.LabelForm(project=project, data={'name': 'Foo'})
        self.assertFalse(form.is_valid(), msg=('Form should be invalid.'))
