"""Tests for the views of the projects app."""
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase

from django_libs.tests.mixins import ViewRequestFactoryTestMixin
from mixer.backend.django import mixer, MOCK_FILE

from .. import views


class ProjectCreateViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.ProjectCreateView

    def setUp(self):
        self.user = mixer.blend('auth.User')

    def test_view(self):
        self.is_callable(user=self.user)
        self.is_postable(user=self.user, data={'name': 'Foo'},
                         to_url_name='project_info')


class ProjectInfoViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.ProjectInfoView

    def setUp(self):
        self.project = mixer.blend('projects.Project')

    def get_view_kwargs(self):
        return {'pk': self.project.pk}

    def test_view(self):
        self.is_callable(user=self.project.owner)
        self.is_postable(user=self.project.owner, to_url_name='project_story',
                         data={
                             'purpose': 'dependency',
                             'type': 'document',
                         })


class ProjectStoryViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.ProjectStoryView

    def setUp(self):
        self.project = mixer.blend('projects.Project')

    def get_view_kwargs(self):
        return {'pk': self.project.pk}

    def test_view(self):
        self.is_callable(user=self.project.owner)
        self.is_postable(user=self.project.owner, data={'description': 'Foo'},
                         to_url_name='project_detail')


class ProjectDetailViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.ProjectDetailView

    def setUp(self):
        self.project = mixer.blend('projects.Project')
        mixer.blend('projects.File', project=self.project)

    def get_view_kwargs(self):
        return {'pk': self.project.pk}

    def test_view(self):
        self.is_callable(user=self.project.owner)


class FileCreateViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.FileCreateView

    def setUp(self):
        self.project = mixer.blend('projects.Project')
        self.user_without_project = mixer.blend('auth.User')

    def get_view_kwargs(self):
        return {'project_pk': self.project.pk}

    def test_view(self):
        self.is_not_callable(user=self.user_without_project)
        self.is_postable(user=self.project.owner, data={}, ajax=True)
        with open(MOCK_FILE) as fp:
            self.file = SimpleUploadedFile('test.txt', '')
            self.file.file = fp
            self.data = {
                'file': self.file,
            }
            self.is_postable(user=self.project.owner, data=self.data,
                             ajax=True)


class FileDeleteViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.FileDeleteView

    def setUp(self):
        self.file = mixer.blend('projects.File')
        self.user_without_project = mixer.blend('auth.User')

    def get_view_kwargs(self):
        return {'pk': self.file.pk}

    def test_view(self):
        self.is_not_callable(user=self.user_without_project)
        self.is_postable(user=self.file.project.owner, data={}, ajax=True)


class InvitationCreateViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.InvitationCreateView

    def setUp(self):
        self.project = mixer.blend('projects.Project')
        self.user_without_project = mixer.blend('auth.User')

    def get_view_kwargs(self):
        return {'project_pk': self.project.pk}

    def test_view(self):
        self.is_not_callable(user=self.user_without_project)
        self.is_callable(user=self.project.owner)
        self.is_postable(user=self.project.owner, data={
            'email': 'foo@example.com'}, to_url_name='invitation_create')


class InvitationAcceptViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.InvitationAcceptView

    def setUp(self):
        self.invitation = mixer.blend('projects.Invitation',
                                      user=mixer.blend('auth.User'))

    def get_view_kwargs(self):
        return {'pk': self.invitation.pk}

    def test_view(self):
        self.redirects(user=self.invitation.user, to_url_name='project_detail')


class InvitationDeclineViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.InvitationDeclineView

    def setUp(self):
        self.invitation = mixer.blend('projects.Invitation',
                                      user=mixer.blend('auth.User'))

    def get_view_kwargs(self):
        return {'pk': self.invitation.pk}

    def test_view(self):
        self.redirects(user=self.invitation.user,
                       to_url_name='invitation_list')
        self.invitation = mixer.blend('projects.Invitation')
        self.redirects(to='/')
        self.redirects(to='/', data={'email': self.invitation.email})


class InvitationListViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.InvitationListView

    def setUp(self):
        self.invitation = mixer.blend('projects.Invitation',
                                      user=mixer.blend('auth.User'))

    def test_view(self):
        self.is_callable(user=self.invitation.user)


class LabelCreateViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.LabelCreateView

    def setUp(self):
        self.project = mixer.blend('projects.Project')
        self.user_without_project = mixer.blend('auth.User')

    def get_view_kwargs(self):
        return {'pk': self.project.pk}

    def test_view(self):
        self.is_not_callable(user=self.project.owner)
        self.is_not_callable(user=self.user_without_project, post=True,
                             ajax=True)
        self.is_postable(user=self.project.owner, data={
            'name': 'Foo'}, ajax=True)


class LabelUpdateViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.LabelUpdateView

    def setUp(self):
        self.label = mixer.blend('projects.Label')
        self.user_without_project = mixer.blend('auth.User')

    def get_view_kwargs(self):
        return {'pk': self.label.pk}

    def test_view(self):
        self.is_not_callable(user=self.label.project.owner)
        self.is_not_callable(user=self.user_without_project, ajax=True)
        self.is_postable(user=self.label.project.owner, data={
            'name': 'Foo'}, ajax=True)


class LabelDeleteViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.LabelDeleteView

    def setUp(self):
        self.label = mixer.blend('projects.Label')
        self.user_without_project = mixer.blend('auth.User')

    def get_view_kwargs(self):
        return {'pk': self.label.pk}

    def test_view(self):
        self.is_callable(user=self.label.project.owner)
        self.is_postable(user=self.label.project.owner,
                         to_url_name='project_detail')


class FileListJSONViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.FileListJSONView

    def setUp(self):
        self.project = mixer.blend('projects.Project')
        mixer.blend('projects.File', project=self.project)

    def get_view_kwargs(self):
        return {'pk': self.project.pk}

    def test_view(self):
        self.redirects(user=self.project.owner, to_url_name='project_detail')
        self.is_callable(user=self.project.owner, ajax=True)


class FileLabelViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.FileLabelView

    def setUp(self):
        self.file = mixer.blend('projects.File')

    def get_view_kwargs(self):
        return {'pk': self.file.pk}

    def test_view(self):
        self.is_callable(user=self.file.project.owner)


class AnnotationCreateViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.AnnotationCreateView

    def setUp(self):
        self.project = mixer.blend('projects.Project')
        self.file = mixer.blend('projects.File', project=self.project)
        self.label = mixer.blend('projects.Label', project=self.project)

    def get_view_kwargs(self):
        return {'file_pk': self.file.pk, 'label_pk': self.label.pk}

    def test_view(self):
        self.is_not_callable(user=self.project.owner, ajax=True)
        self.is_not_callable(user=self.project.owner, ajax=True, post=True,
                             kwargs={'file_pk': self.file.pk, 'label_pk': 999})
        self.is_not_callable(user=self.project.owner, ajax=True, post=True,
                             kwargs={'file_pk': 99, 'label_pk': self.label.pk})
        self.is_postable(user=self.project.owner, ajax=True, data={
            'sentence': 1,
            'first_word': 'Foo',
            'first_word_position': 3,
            'second_word': 'Bar',
            'second_word_position': 7,
        })


class AnnotationDeleteViewTestCase(ViewRequestFactoryTestMixin, TestCase):
    view_class = views.AnnotationDeleteView

    def setUp(self):
        self.annotation = mixer.blend('projects.Annotation')

    def get_view_kwargs(self):
        return {'pk': self.annotation.pk}

    def test_view(self):
        self.is_not_callable(user=self.annotation.label.project.owner,
                             ajax=True)
        self.is_postable(user=self.annotation.label.project.owner, ajax=True)
