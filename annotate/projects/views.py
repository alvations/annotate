"""Views for the projects app."""
import json
import mimetypes
import os

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.files.base import ContentFile
from django.core.urlresolvers import reverse
from django.http import (
    Http404, HttpResponse, HttpResponseRedirect, JsonResponse)
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.views.generic import (
    CreateView, DeleteView, DetailView, ListView, UpdateView)

from django_libs.utils.email import send_email

from . import forms
from . import models


class ProjectViewMixin(object):
    model = models.Project

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ProjectViewMixin, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.request.user.projects.all()


class ProjectCreateView(ProjectViewMixin, CreateView):
    form_class = forms.ProjectForm

    def get_form_kwargs(self):
        kwargs = super(ProjectCreateView, self).get_form_kwargs()
        kwargs.update({'owner': self.request.user})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(ProjectCreateView, self).get_context_data(**kwargs)
        context.update({'step': 'create'})
        return context

    def get_success_url(self):
        return reverse('project_info', kwargs={'pk': self.object.pk})


class ProjectInfoView(ProjectViewMixin, UpdateView):
    form_class = forms.ProjectInfoForm

    def get_context_data(self, **kwargs):
        context = super(ProjectInfoView, self).get_context_data(**kwargs)
        context.update({'step': 'info'})
        return context

    def get_success_url(self):
        return reverse('project_story', kwargs={'pk': self.object.pk})


class ProjectStoryView(ProjectViewMixin, UpdateView):
    fields = ['description']

    def get_context_data(self, **kwargs):
        context = super(ProjectStoryView, self).get_context_data(**kwargs)
        context.update({'step': 'story'})
        return context

    def get_success_url(self):
        return reverse('project_detail', kwargs={'pk': self.object.pk})


class LabelCreateView(CreateView):
    model = models.Label
    form_class = forms.LabelForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.is_ajax() or request.method != 'POST':
            raise Http404
        try:
            self.project = self.request.user.projects.get(pk=kwargs['pk'])
        except models.Project.DoesNotExist:
            raise Http404
        return super(LabelCreateView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(LabelCreateView, self).get_form_kwargs()
        kwargs.update({'project': self.project})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(LabelCreateView, self).get_context_data(**kwargs)
        context.update({'project': self.project})
        return context

    def form_valid(self, form):
        self.object = form.save()
        context = self.get_context_data(form=form)
        context.update({'add_empty_form': True})
        return self.render_to_response(context)


class LabelUpdateView(UpdateView):
    model = models.Label
    form_class = forms.LabelForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.is_ajax() or request.method != 'POST':
            raise Http404
        return super(LabelUpdateView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(LabelUpdateView, self).get_context_data(**kwargs)
        context.update({'project': self.object.project})
        return context

    def get_form_kwargs(self):
        kwargs = super(LabelUpdateView, self).get_form_kwargs()
        kwargs.update({'project': self.object.project})
        return kwargs

    def get_queryset(self):
        return models.Label.objects.filter(
            project__in=self.request.user.projects.all())

    def form_valid(self, form):
        self.object = form.save()
        return self.render_to_response(self.get_context_data(form=form))


class LabelDeleteView(DeleteView):
    model = models.Label

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LabelDeleteView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(LabelDeleteView, self).get_context_data(**kwargs)
        context.update({
            'project': self.object.project,
            'hide_label_forms': True,
        })
        return context

    def get_queryset(self):
        return models.Label.objects.filter(
            project__in=self.request.user.projects.all())

    def get_success_url(self):
        return reverse('project_detail', kwargs={'pk': self.object.project.pk})


def serialize(instance, file_attr='file'):
    """serialize -- Serialize a Picture instance into a dict.
    instance -- Picture instance
    file_attr -- attribute name that contains the FileField or ImageField
    """
    obj = getattr(instance, file_attr)
    return {
        'url': obj.url,
        'name': instance.get_name(),
        'type': mimetypes.guess_type(obj.path)[0] or 'text/plain',
        'size': obj.size,
        'deleteUrl': reverse('file_delete', kwargs={'pk': instance.pk}),
        'labelUrl': reverse('file_label', kwargs={'pk': instance.pk}),
        'deleteType': 'DELETE',
    }


class ProjectDetailView(ProjectViewMixin, DetailView):
    def get_queryset(self):
        return (self.request.user.projects.all() |
                self.request.user.projects_contributed.all()).distinct()


class FileListJSONView(ProjectViewMixin, DetailView):
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if request.is_ajax():
            response = JsonResponse({
                'files': [serialize(f) for f in self.object.files.all()],
            })
            response['Content-Disposition'] = 'inline; filename=files.json'
            return response
        return HttpResponseRedirect(
            reverse('project_detail', kwargs={'pk': self.object.pk}))


class FileCreateView(CreateView):
    form_class = forms.FileForm
    model = models.File

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        try:
            self.project = request.user.projects.get(pk=kwargs['project_pk'])
        except models.Project.DoesNotExist:
            raise Http404
        return super(FileCreateView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(FileCreateView, self).get_form_kwargs()
        kwargs.update({'project': self.project})
        return kwargs

    def form_valid(self, form):
        self.object = form.save()
        content = self.object.file.read()

        # Get sentences
        sentences = [s for s in content.split('\n') if s.strip(' \t\n\r')]
        # Split sentences into chunks of 1000
        chunk_size = 1000
        chunks = [sentences[x:x + chunk_size] for x in xrange(
            0, len(sentences), chunk_size)]

        counter = 1
        filename_tuple = os.path.splitext(self.object.file.name)
        # Save first chunk to original file
        self.object.file.save(
            u'{}_{}{}'.format(filename_tuple[0], counter, filename_tuple[1]),
            ContentFile('\n'.join(chunks.pop(0))))
        files = [serialize(self.object)]
        if chunks:  # pragma: nocover
            messages.add_message(self.request, messages.SUCCESS, _(
                'We\'ve automatically split your file into {} parts.').format(
                len(chunks) + 1))
        while chunks:  # pragma: nocover
            counter += 1
            # Create new files for each chunk
            file = models.File.objects.create(project=self.object.project)
            file.file.save(u'{}_{}{}'.format(
                filename_tuple[0], counter, filename_tuple[1]),
                ContentFile('\n'.join(chunks.pop(0))))
            files.append(serialize(file))
        response = JsonResponse({'files': files})
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response

    def form_invalid(self, form):
        return JsonResponse(form.errors)


class FileLabelView(DetailView):
    model = models.File

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(FileLabelView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        projects = (self.request.user.projects.all() |
                    self.request.user.projects_contributed.all()).distinct()
        return models.File.objects.filter(project__in=projects)

    def get_context_data(self, **kwargs):
        context = super(FileLabelView, self).get_context_data(**kwargs)
        sentences = [s for s in self.object.file.read().split('\n') if s.strip(
            ' \t\n\r')]
        tokenized_sentences = []
        for i, sentence in enumerate(sentences, start=1):
            tokenized_sentences.append({
                'tokens': sentence.split(),
                'annotations': json.dumps(
                    [{
                         'pk': a.pk,
                         'first_word': a.first_word_position,
                         'second_word': a.second_word_position,
                         'label': a.label.pk,
                         'delete_url': reverse('annotation_delete', kwargs={
                             'pk': a.pk}),
                    } for a in self.object.annotations.filter(sentence=i)]),
            })
        context.update({
            'project': self.object.project,
            'sentences': tokenized_sentences,
            'hide_label_forms': True,
        })
        return context


class FileDeleteView(DeleteView):
    model = models.File

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        self.object = self.get_object()
        if self.object.project.owner != request.user:
            raise Http404
        return super(FileDeleteView, self).dispatch(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        response = JsonResponse({'files': True})
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class InvitationCreateView(CreateView):
    form_class = forms.InvitationForm
    model = models.Invitation

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        try:
            self.project = request.user.projects.get(pk=kwargs['project_pk'])
        except models.Project.DoesNotExist:
            raise Http404
        return super(InvitationCreateView, self).dispatch(
            request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(InvitationCreateView, self).get_context_data(**kwargs)
        context.update({'project': self.project})
        return context

    def get_form_kwargs(self):
        kwargs = super(InvitationCreateView, self).get_form_kwargs()
        kwargs.update({'project': self.project})
        return kwargs

    def form_valid(self, form):
        resp = super(InvitationCreateView, self).form_valid(form)
        send_email(
            self.request,
            {'invitation': self.object},
            'projects/email/invitation_subject.html',
            'projects/email/invitation_body.html',
            settings.FROM_EMAIL,
            [form.cleaned_data['email']],
        )
        return resp

    def get_success_url(self):
        return reverse('invitation_create', kwargs={
            'project_pk': self.project.pk})


class InvitationAcceptView(UpdateView):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        self.object = self.get_object(self.request.user.invitations.filter(
            status='pending'))
        self.object.status = 'accepted'
        self.object.save()
        # Add user to contributors
        self.object.project.contributors.add(self.object.user)
        return HttpResponseRedirect(
            reverse('project_detail', kwargs={'pk': self.object.project.pk}))


class InvitationDeclineView(UpdateView):
    model = models.Invitation

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        if request.user.is_authenticated():
            self.object = self.get_object(self.request.user.invitations.filter(
                status='pending'))
            self.object.status = 'declined'
            self.object.save()
            return HttpResponseRedirect(reverse('invitation_list'))
        else:
            self.object = self.get_object(
                models.Invitation.objects.filter(status='pending'))
            if self.object.email != request.GET.get('email'):
                messages.add_message(request, messages.ERROR, _(
                    'Please use the link, we\'ve sent you via email.'))
            else:
                messages.add_message(request, messages.WARNING, _(
                    'The invitation has been cancelled.'))
                self.object.status = 'declined'
                self.object.save()
        return HttpResponseRedirect('/')


class InvitationListView(ListView):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(InvitationListView, self).dispatch(
            request, *args, **kwargs)

    def get_queryset(self):
        return self.request.user.invitations.all()


class AnnotationCreateView(CreateView):
    model = models.Annotation
    fields = ['sentence', 'first_word', 'first_word_position', 'second_word',
              'second_word_position']

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.is_ajax() or request.method != 'POST':
            raise Http404
        projects = (self.request.user.projects.all() |
                    self.request.user.projects_contributed.all()).distinct()
        try:
            self.file = models.File.objects.get(
                project__in=projects, pk=kwargs['file_pk'])
        except models.File.DoesNotExist:
            raise Http404
        try:
            self.label = models.Label.objects.get(
                project__in=projects, pk=kwargs['label_pk'])
        except models.Label.DoesNotExist:
            raise Http404
        return super(AnnotationCreateView, self).dispatch(
            request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.file = self.file
        form.instance.label = self.label
        self.object = form.save()
        return HttpResponse(json.dumps({
            'label_pk': self.object.label.pk,
            'pk': self.object.pk,
            'delete_url': reverse('annotation_delete', kwargs={
                'pk': self.object.pk}),
        }))


class AnnotationDeleteView(DeleteView):
    model = models.Annotation

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.is_ajax() or request.method != 'POST':
            raise Http404
        return super(AnnotationDeleteView, self).dispatch(
            request, *args, **kwargs)

    def get_queryset(self):
        projects = (self.request.user.projects.all() |
                    self.request.user.projects_contributed.all()).distinct()
        return models.Annotation.objects.filter(label__project__in=projects)

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        return HttpResponse('ok')
