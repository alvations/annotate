"""Forms for the projects app."""
from django.contrib.auth import get_user_model
from django import forms
from django.utils.translation import ugettext_lazy as _

from . import models


class ProjectForm(forms.ModelForm):
    class Meta:
        fields = ['name']
        model = models.Project

    def __init__(self, owner, *args, **kwargs):
        self.owner = owner
        super(ProjectForm, self).__init__(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.instance.owner = self.owner
        return super(ProjectForm, self).save(*args, **kwargs)


class ProjectInfoForm(forms.ModelForm):
    class Meta:
        fields = ['type', 'deadline', 'purpose']
        model = models.Project
        widgets = {
            'deadline': forms.SelectDateWidget(attrs={
                'style': 'display: inline-block; width: 70px;'}),
        }


class FileForm(forms.ModelForm):
    class Meta:
        fields = ['file']
        model = models.File

    def __init__(self, project, *args, **kwargs):
        self.project = project
        super(FileForm, self).__init__(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.instance.project = self.project
        return super(FileForm, self).save(*args, **kwargs)


class InvitationForm(forms.ModelForm):
    class Meta:
        fields = ['email', 'first_name', 'last_name']
        model = models.Invitation

    def __init__(self, project, *args, **kwargs):
        self.project = project
        super(InvitationForm, self).__init__(*args, **kwargs)

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if email and self.project.invitations.filter(email=email):
            raise forms.ValidationError(
                _('You\'ve already invited this user.'))
        if email and email == self.project.owner.email:
            raise forms.ValidationError(
                _('You cannot invite yourself.'))
        return email

    def save(self, *args, **kwargs):
        self.instance.project = self.project
        User = get_user_model()
        try:
            user = User.objects.get(email=self.cleaned_data['email'])
        except User.DoesNotExist:
            pass
        else:
            self.instance.user = user
        return super(InvitationForm, self).save(*args, **kwargs)


class LabelForm(forms.ModelForm):
    class Meta:
        fields = ['name']
        model = models.Label

    def __init__(self, project, *args, **kwargs):
        self.project = project
        super(LabelForm, self).__init__(*args, **kwargs)

    def clean_name(self):
        name = self.cleaned_data.get('name')
        if self.project.labels.exclude(pk=self.instance.pk).filter(name=name):
            raise forms.ValidationError(_('Label already exists.'))
        return name

    def save(self, *args, **kwargs):
        if not self.instance.pk:
            self.instance.project = self.project
        return super(LabelForm, self).save(*args, **kwargs)
