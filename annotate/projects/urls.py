"""URLs for the projects app."""
from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^invitations/(?P<pk>\d+)/decline/$',
        views.InvitationDeclineView.as_view(),
        name='invitation_decline'),
    url(r'^invitations/(?P<pk>\d+)/accept/$',
        views.InvitationAcceptView.as_view(),
        name='invitation_accept'),
    url(r'^invitations/$', views.InvitationListView.as_view(),
        name='invitation_list'),
    url(r'^(?P<project_pk>\d+)/invitations/$',
        views.InvitationCreateView.as_view(),
        name='invitation_create'),
    url(r'^file/annotation/(?P<pk>\d+)/delete/$',
        views.AnnotationDeleteView.as_view(), name='annotation_delete'),
    url(r'^file/(?P<file_pk>\d+)/label/(?P<label_pk>\d+)/$',
        views.AnnotationCreateView.as_view(), name='annotation_create'),
    url(r'^file/(?P<pk>\d+)/label/$', views.FileLabelView.as_view(),
        name='file_label'),
    url(r'^file/(?P<pk>\d+)/delete/$', views.FileDeleteView.as_view(),
        name='file_delete'),
    url(r'^(?P<project_pk>\d+)/file/create/$', views.FileCreateView.as_view(),
        name='file_create'),
    url(r'^(?P<pk>\d+)/story/$', views.ProjectStoryView.as_view(),
        name='project_story'),
    url(r'^(?P<pk>\d+)/info/$', views.ProjectInfoView.as_view(),
        name='project_info'),
    url(r'^label/(?P<pk>\d+)/delete/$', views.LabelDeleteView.as_view(),
        name='label_delete'),
    url(r'^label/(?P<pk>\d+)/update/$', views.LabelUpdateView.as_view(),
        name='label_update'),
    url(r'^(?P<pk>\d+)/label/create/$', views.LabelCreateView.as_view(),
        name='label_create'),
    url(r'^(?P<pk>\d+)/files/$', views.FileListJSONView.as_view(),
        name='file_json'),
    url(r'^(?P<pk>\d+)/$', views.ProjectDetailView.as_view(),
        name='project_detail'),
    url(r'^create/$', views.ProjectCreateView.as_view(),
        name='project_create'),
]
