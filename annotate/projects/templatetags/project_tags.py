"""Template tags for the projects app."""
from django import template
from django.utils.safestring import mark_safe


register = template.Library()


@register.simple_tag
def upload_js():
    return mark_safe("""
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload">
        <td>
            <p class="name">{%=file.name%}</p>
            {% if (file.error) { %}
                <div>
                    <span class="label label-important">
                        {%=locale.fileupload.error%}
                    </span> {%=file.error%}
                </div>
            {% } %}
        </td>
        <td>
            <p class="size">{%=o.formatFileSize(file.size)%}</p>
        </td>
        <td style="min-width: 100px;">
            {% if (!o.files.error) { %}
                <div class="progress progress-striped active"
                role="progressbar" aria-valuemin="0" aria-valuemax="100"
                aria-valuenow="0">
                    <div class="progress-bar progress-bar-success"
                    style="width:0%;"></div>
                </div>
            {% } %}
        </td>
        <td class="text-right">
            {% if (!o.files.error && !i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start">
                    <span>{%=locale.fileupload.start%}</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <span>{%=locale.fileupload.cancel%}</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download">
        <td>
            <p class="name">
                <a href="{%=file.url%}" title="{%=file.name%}"
                download="{%=file.name%}"
                {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
            </p>
            {% if (file.error) { %}
                <div>
                    <span class="label label-important">
                        {%=locale.fileupload.error%}
                    </span> {%=file.error%}
                </div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td></td>
        <td class="text-right">
            <a class="btn btn-warning"
            href="{%=file.labelUrl%}">
                <span>{%=locale.fileupload.label%}</span>
            </a>
            <button class="btn btn-danger delete"
            data-type="{%=file.deleteType%}"
            data-url="{%=file.deleteUrl%}"
            {% if (file.deleteWithCredentials) { %}
            data-xhr-fields='{"withCredentials":true}'{% } %}>
                <i class="glyphicon glyphicon-trash"></i>
                <span>{%=locale.fileupload.destroy%}</span>
            </button>
        </td>
    </tr>
{% } %}
</script>
""")
