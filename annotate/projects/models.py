"""Models for the project view."""
import re

from django.conf import settings
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


@python_2_unicode_compatible
class Project(models.Model):
    """
    A project contains documents and labels.

    :name: Name of the project.
    :owner: User, who owns this project.
    :type: Type of the project.
    :deadline: Project deadline.
    :purpose: Purpose of the project.
    :description: Long and detailed project description.
    :contributors: Users, who contribute to this project.

    """
    name = models.CharField(
        max_length=256,
        verbose_name=_('Name'),
    )

    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Owner'),
        related_name='projects',
    )

    type = models.CharField(
        max_length=256,
        verbose_name=_('Project type'),
        choices=(
            ('document', _('Document labelling')),
            ('edge', _('Edge labelling')),
            ('span', _('Span labelling')),
        )
    )

    deadline = models.DateField(
        blank=True, null=True,
        verbose_name=_('Deadline'),
    )

    purpose = models.CharField(
        max_length=256,
        verbose_name=_('Purpose'),
        choices=(
            ('pos', _('POS tagging')),
            ('sentiment', _('Sentiment')),
            ('dependency', _('Dependency')),
        )
    )

    description = models.TextField(
        max_length=2048,
        verbose_name=_('Description'),
    )

    contributors = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Contributors'),
        related_name='projects_contributed',
        blank=True,
    )

    class Meta:
        verbose_name = _('Project')
        verbose_name_plural = _('Projects')
        ordering = ['name']

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class File(models.Model):
    """
    File object, which can be assigned to a project.

    :file: File instance.
    :project: Project, the file should be assigned to.
    """
    file = models.FileField(
        verbose_name=_('File'),
        upload_to="project_files",
    )

    project = models.ForeignKey(
        'projects.Project',
        verbose_name=_('Project'),
        related_name='files',
    )

    class Meta:
        verbose_name = _('File')
        verbose_name_plural = _('Files')
        ordering = ['-pk']

    def __str__(self):
        return self.file.name

    def delete(self, *args, **kwargs):
        self.file.delete(False)
        super(File, self).delete(*args, **kwargs)

    def get_name(self):
        name = re.sub(r'^.*/', '', self.file.name)
        if len(name) > 20:  # pragma: nocover
            name = name[:10] + "..." + name[-7:]
        return name


@python_2_unicode_compatible
class Invitation(models.Model):
    """
    User invitation to contribute to a project.

    :project: Related project.
    :user: Connected user account.
    :email: Email of the invited person.
    :first_name: Optional first name of the invited person.
    :last_name: Optional last name of the invited person.
    :status: Status of the invitation.
    """
    project = models.ForeignKey(
        'projects.Project',
        verbose_name=_('Project'),
        related_name='invitations',
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('User'),
        related_name='invitations',
        blank=True, null=True,
    )

    email = models.EmailField(
        verbose_name=_('Email'),
    )

    first_name = models.CharField(
        max_length=256,
        verbose_name=_('First name'),
        blank=True,
    )

    last_name = models.CharField(
        max_length=256,
        verbose_name=_('Last name'),
        blank=True,
    )

    status = models.CharField(
        max_length=50,
        verbose_name=_('Status'),
        choices=(
            ('pending', _('Pending')),
            ('accepted', _('Accepted')),
            ('declined', _('Declined')),
        ),
        default='pending',
    )

    class Meta:
        verbose_name = _('Invitation')
        verbose_name_plural = _('Invitations')
        ordering = ['email']

    def __str__(self):
        return self.email


@python_2_unicode_compatible
class Label(models.Model):
    """
    Label, which can be used to label a document.

    :name: Name of the label.
    :project: Project, the label should be assigned to.
    :colour: Colour of the label.
    """
    name = models.CharField(
        max_length=56,
        verbose_name=_('Name'),
    )

    project = models.ForeignKey(
        'projects.Project',
        verbose_name=_('Project'),
        related_name='labels',
    )

    colour = models.CharField(
        verbose_name=_('Colour'),
        max_length=7,
    )

    class Meta:
        verbose_name = _('Label')
        verbose_name_plural = _('Labels')
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.colour:
            colours = {
                'antiquewhite': '#FAEBD7',
                'aqua': '#00FFFF',
                'aquamarine': '#7FFFD4',
                'azure': '#F0FFFF',
                'beige': '#F5F5DC',
                'bisque': '#FFE4C4',
                'black': '#000000',
                'blanchedalmond': '#FFEBCD',
                'blue': '#0000FF',
                'blueviolet': '#8A2BE2',
                'brown': '#A52A2A',
                'burlywood': '#DEB887',
                'cadetblue': '#5F9EA0',
                'chartreuse': '#7FFF00',
                'chocolate': '#D2691E',
                'coral': '#FF7F50',
                'cornflowerblue': '#6495ED',
                'cornsilk': '#FFF8DC',
                'crimson': '#DC143C',
                'cyan': '#00FFFF',
                'darkblue': '#00008B',
                'darkcyan': '#008B8B',
                'darkgoldenrod': '#B8860B',
                'darkgray': '#A9A9A9',
                'darkgreen': '#006400',
                'darkkhaki': '#BDB76B',
                'darkmagenta': '#8B008B',
                'darkolivegreen': '#556B2F',
                'darkorange': '#FF8C00',
                'darkorchid': '#9932CC',
                'darkred': '#8B0000',
                'darksalmon': '#E9967A',
                'darkseagreen': '#8FBC8F',
                'darkslateblue': '#483D8B',
                'darkslategray': '#2F4F4F',
                'darkturquoise': '#00CED1',
                'darkviolet': '#9400D3',
                'deeppink': '#FF1493',
                'deepskyblue': '#00BFFF',
                'dimgray': '#696969',
                'dodgerblue': '#1E90FF',
                'firebrick': '#B22222',
                'floralwhite': '#FFFAF0',
                'forestgreen': '#228B22',
                'fuchsia': '#FF00FF',
                'gainsboro': '#DCDCDC',
                'ghostwhite': '#F8F8FF',
                'gold': '#FFD700',
                'goldenrod': '#DAA520',
                'gray': '#808080',
                'green': '#008000',
                'greenyellow': '#ADFF2F',
                'honeydew': '#F0FFF0',
                'hotpink': '#FF69B4',
                'indianred': '#CD5C5C',
                'indigo': '#4B0082',
                'ivory': '#FFFFF0',
                'khaki': '#F0E68C',
                'lavender': '#E6E6FA',
                'lavenderblush': '#FFF0F5',
                'lawngreen': '#7CFC00',
                'lemonchiffon': '#FFFACD',
                'lightblue': '#ADD8E6',
                'lightcoral': '#F08080',
                'lightcyan': '#E0FFFF',
                'lightgoldenrodyellow': '#FAFAD2',
                'lightgreen': '#90EE90',
                'lightgray': '#D3D3D3',
                'lightpink': '#FFB6C1',
                'lightsalmon': '#FFA07A',
                'lightseagreen': '#20B2AA',
                'lightskyblue': '#87CEFA',
                'lightslategray': '#778899',
                'lightsteelblue': '#B0C4DE',
                'lightyellow': '#FFFFE0',
                'lime': '#00FF00',
                'limegreen': '#32CD32',
                'linen': '#FAF0E6',
                'magenta': '#FF00FF',
                'maroon': '#800000',
                'mediumaquamarine': '#66CDAA',
                'mediumblue': '#0000CD',
                'mediumorchid': '#BA55D3',
                'mediumpurple': '#9370DB',
                'mediumseagreen': '#3CB371',
                'mediumslateblue': '#7B68EE',
                'mediumspringgreen': '#00FA9A',
                'mediumturquoise': '#48D1CC',
                'mediumvioletred': '#C71585',
                'midnightblue': '#191970',
                'mintcream': '#F5FFFA',
                'mistyrose': '#FFE4E1',
                'moccasin': '#FFE4B5',
                'navajowhite': '#FFDEAD',
                'navy': '#000080',
                'oldlace': '#FDF5E6',
                'olive': '#808000',
                'olivedrab': '#6B8E23',
                'orange': '#FFA500',
                'orangered': '#FF4500',
                'orchid': '#DA70D6',
                'palegoldenrod': '#EEE8AA',
                'palegreen': '#98FB98',
                'paleturquoise': '#AFEEEE',
                'palevioletred': '#DB7093',
                'papayawhip': '#FFEFD5',
                'peachpuff': '#FFDAB9',
                'peru': '#CD853F',
                'pink': '#FFC0CB',
                'plum': '#DDA0DD',
                'powderblue': '#B0E0E6',
                'purple': '#800080',
                'red': '#FF0000',
                'rosybrown': '#BC8F8F',
                'royalblue': '#4169E1',
                'saddlebrown': '#8B4513',
                'salmon': '#FA8072',
                'sandybrown': '#FAA460',
                'seagreen': '#2E8B57',
                'seashell': '#FFF5EE',
                'sienna': '#A0522D',
                'silver': '#C0C0C0',
                'skyblue': '#87CEEB',
                'slateblue': '#6A5ACD',
                'slategray': '#708090',
                'snow': '#FFFAFA',
                'springgreen': '#00FF7F',
                'steelblue': '#4682B4',
                'tan': '#D2B48C',
                'teal': '#008080',
                'thistle': '#D8BFD8',
                'tomato': '#FF6347',
                'turquoise': '#40E0D0',
                'violet': '#EE82EE',
                'wheat': '#F5DEB3',
                'white': '#FFFFFF',
                'whitesmoke': '#F5F5F5',
                'yellow': '#FFFF00',
                'yellowgreen': '#9ACD32',
            }
            # There are only 139 colours available
            # We need to make sure, that every label get a colour
            # E.g. if there are 175 labels,
            count = self.project.labels.count()
            self.colour = list(colours.values())[
                count - (len(colours) * int(count / len(colours)))]
        return super(Label, self).save(*args, **kwargs)


@python_2_unicode_compatible
class Annotation(models.Model):
    """
    Label, which contains information about an annotation.

    :user: User, who created the annotation.
    :file: Related file.
    :label: Label, which has been used for the annotation.
    :sentence: Sentence, the annotation has been added to.
    :first_word: First marked word.
    :first_word_position: Position of the first word in the relevant sentence.
    :second_word: Second marked word.
    :second_word_position:
      Position of the second word in the relevant sentence.
    """
    user = models.ForeignKey(
        'auth.User',
        verbose_name=_('User'),
        related_name='annotations',
    )

    file = models.ForeignKey(
        'projects.File',
        verbose_name=_('File'),
        related_name='annotations',
    )

    label = models.ForeignKey(
        'projects.Label',
        verbose_name=_('Label'),
        related_name='annotations',
    )

    sentence = models.PositiveIntegerField(
        verbose_name=_('Sentence'),
    )

    first_word = models.CharField(
        verbose_name=_('First word'),
        max_length=128,
    )

    first_word_position = models.PositiveIntegerField(
        verbose_name=_('First word position'),
    )

    second_word = models.CharField(
        verbose_name=_('Second word'),
        max_length=128,
    )

    second_word_position = models.PositiveIntegerField(
        verbose_name=_('Second word position'),
    )

    class Meta:
        verbose_name = _('Annotation')
        verbose_name_plural = _('Annotations')
        ordering = ['-pk']

    def __str__(self):
        return u'{}, {} - {}'.format(
            self.first_word, self.second_word, self.label.name)
